$(function() {

	//切换搜索类别
	$("#search-type").change(function(){
		var labelValue = $(this).val(),
			labelText = $(this).find('option[value="' + labelValue + '"]').text();
		$(".search-type-label").text(labelText);
	});

	//切换语言
	$("#lang-list").change(function(){
		var labelValue = $(this).val(),
			labelText = $(this).find('option[value="' + labelValue + '"]').text();
		$(".lang-list-label").text(labelText);
		// $.get(
		// 	"/switch?v=" + labelValue,
		// 	function(data) {
		// 		location.reload();
		// 	}
		// )
		location.reload();
	});
})