#coding:utf-8
from app.handler.base import BaseHandler
from app.model.search import SearchModel


#搜索处理Handler
class SearchHandler(BaseHandler, SearchModel):
    def get(self):
        type = self.get_argument("t")
        keywords = self.get_argument("k")
        
        #####从倒排索引中寻找匹配的关键词
        sObject = SearchModel()
        resultList = sObject.getSearchResult(keywords)

        self.render('result.rst', resultList=resultList, keywords=keywords)