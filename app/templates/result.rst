<!doctype html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>${keywords} - Catty.IO</title>
	
	<meta name="description" content="Catty.IO —— 最专业的资源搜索站点">
	<meta name="keywords" content="archived, magnet, search, bt, torrent, magnet, convert, imagekitty, torrent2magnet, bittorrent, pdf, download, mp4, avi">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="/static/css/public.css?v=20150516" type="text/css" rel="stylesheet" />
	<link href="/static/css/result.css?v=20150516" type="text/css" rel="stylesheet" />
</head>
<body>
	<%include file="header.rst" />
	<div class="container">
		<form class="site-search" method="get" action="/s">
			<div class="search-type-wrap">
				<label class="search-type-label" for="search-type">全部</label>
				<select class="search-type" id="search-type" name="t">
					<option value="1">全部</option>
					<option value="2">doc</option>
					<option value="3">pdf</option>
					<option value="4">ppt</option>
					<option value="5">txt</option>
					<option value="6">视频</option>
				</select>
			</div>
			<input type="text" name="k" value="${keywords}" placeholder="输入文件关键词" class="search-input" />
			<input type="submit" class="search-btn" value="搜索">
			<div class="clearfix"></div>
		</form>
		<div class="site-main">
			<div class="result-list">
				<div class="result-count-label">找到 <b>${len(resultList)}</b> 个结果</div>
				<ul>
					% for i,item in enumerate(resultList):
						<li class="list-item">
							<a class="item-title" href="#"  title="${item.get('res_name')}">${item.get('res_name')}</a>
							<div class="item-detail">
							<span class="item-label">收录时间：</span><span class="item-value">${item.get('ctime')}</span>
							<span class="item-label">资源大小：</span><span class="item-value">${item.get('res_size')}</span>
							<!--a href="${item.get('web_link')}" class="item-origin-weblink" target="_blank">源网址</a-->
							<span class="item-label">资源速度：</span><span class="item-value">${item.get('res_speed')}</span>
							<span class="item-label">人气：</span><span class="item-value">${item.get('visited')}</span>
							<a class="item-download" href="${item.get('res_link')}">下载</a>
							<div class="clearfix"></div>
							</div>
						</li>
					% endfor
				</ul>
			</div>
			<div class="sidebar-wrap"></div>
		</div>
	</div>
	<%include file="footer.rst" />
</body>
</html>