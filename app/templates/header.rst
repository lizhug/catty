<div class="site-header">
	<div class="container">
		<ul class="header-menu pull-left">
			<li><a href="/">首页</a></li>
			<!-- <li><a href="#">提交地址</a></li> -->
		</ul>
		<ul class="header-menu pull-right">
			<li class="lang-swith">
				<div class="lang-list-wrap">
					<label for="lang-list" class="lang-list-label">简体中文</label>
					<select class="lang-list" id="lang-list">
						<option value="cn">简体中文</option>
						<option value="tw">繁体中文</option>
						<option value="en">English</option>
					</select>
				</div>
			</li>
		</ul>
	</div>
</div>