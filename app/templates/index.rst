<!doctype html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Catty.IO - 最专业的资源搜索站点</title>
	
	<meta name="description" content="Catty.IO —— 最专业的资源搜索站点">
	<meta name="keywords" content="archived, magnet, search, bt, torrent, magnet, convert, imagekitty, torrent2magnet, bittorrent, pdf, download, mp4, avi">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="/static/css/public.css?v=20150516" type="text/css" rel="stylesheet" />
	<link href="/static/css/home.css?v=20150516" type="text/css" rel="stylesheet" />
</head>
<body>
	<%include file="header.rst" />
	<div class="container">
		<div class="site-banner">Catty.IO</div>
		<form class="site-search" method="get" action="/s">
			<div class="search-type-wrap">
				<label class="search-type-label" for="search-type">全部</label>
				<select class="search-type" id="search-type" name="t">
					<option value="1">全部</option>
					<option value="2">doc</option>
					<option value="3">pdf</option>
					<option value="4">ppt</option>
					<option value="5">txt</option>
					<option value="6">视频</option>
				</select>
			</div>
			<input type="text" name="k" value="" autocomplete="false" placeholder="输入文件关键词" class="search-input"/>
			<input type="submit" class="search-btn" value="搜索">
			<div class="clearfix"></div>
		</form>
	</div>
	<%include file="footer.rst" />
</body>
</html>