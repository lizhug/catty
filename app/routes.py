from app.handler.index import MainHandler
from app.handler.search import SearchHandler

routes = [
	(r'/', MainHandler),
	(r'/s', SearchHandler),
]