#coding:utf-8
import jieba
import redis
import sqlite3
import json

r_server = redis.Redis("127.0.0.1")

class SearchModel():

	def getSearchResult(self, keywords):
		#jieba分词
		keyword_line = list(set(w.upper() for w in jieba.cut(keywords)))
		
		#print keyword_line
		#####从倒排索引中寻找匹配的关键词、写一个数据结构，从redis中取数据进行结构化
        #### 1. 根据单词匹配的数量排序
		id_tmp = {}
		for words in keyword_line:
			id_list = list(r_server.smembers("key:" + words + ":id"))
			#rint id_list
			for index in id_list:
				index = int(index)			###str to int
				if index in id_tmp:
					id_tmp[index] += 1
				else:
					id_tmp[index] = 1


		#根据关键词递减排列的id列表
		id_tmp = sorted(id_tmp.items(), key=lambda d:d[1], reverse = True)

        #链接数据库取数据
		conn = sqlite3.connect("test.db")
		conn.row_factory = sqlite3.Row
		cu = conn.cursor()
		result = []

		for m_id in id_tmp:
			cu.execute("select * from res where id=?", (int(m_id[0]),))
			result = result + cu.fetchall()

		cu.close()
		conn.close()

		#返回的页面数据
		returnData = []

		for item in result:

			##资源速度
			speed_label = "未知"
			speed_value = int(item['res_speed'])

			if speed_value == 0:
				speed_label = "中等"
			elif speed_value > 500:
				speed_label = "极快"
			elif speed_value > 200:
				speed_label = "快"
			elif speed_value > 100:
				speed_label = "中等"
			elif speed_value > 50:
				speed_label = "慢"

			##资源大小
			res_size = int(item['res_size'])
			if res_size < 1024:
				res_size = str(round(res_size, 1)) + "KB"
			elif res_size < 1024 * 1024:
				res_size = str(round(res_size / 1024, 1)) + "M"
			elif res_size < 1024 * 1024 * 1024:
				res_size = str(round(res_size / 1024 / 1024, 1)) + "G"

			##关键词高亮替换
			resName = item['res_name']
			for m_key in keyword_line:
				resName = resName.replace(m_key, "<strong>" + m_key + "</strong>")

			returnData.append({
				"type": item['type'],
				"res_name": resName,
				"res_link": item['res_link'],
				"res_size": res_size,
				"res_speed": speed_label,
				"web_link": item['web_link'],
				"visited": item['visited'],
				"ctime": item['ctime'][:10]
			})

		return returnData