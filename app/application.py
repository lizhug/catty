import tornado.web
import os
from app.routes import routes

#全局设置
settings = {
    'template_path': os.path.join(os.path.dirname(__file__), 'templates'),
    'static_path': os.path.join(os.path.dirname(__file__), 'static'),
    'gzip': True,
}

application = tornado.web.Application(
    handlers=routes, 
    **settings
)