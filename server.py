import tornado.ioloop
from app.application import application
from tornado.options import define, options
import sys
import os
import sqlite3

###创建数据库
if os.path.exists("test.db"):
	pass
else:
	s_conn = sqlite3.connect("test.db")
	cursor = s_conn.cursor()

	try:
		cursor.execute("create table res(id integer PRIMARY KEY autoincrement, type integer, res_name text, res_link text, res_size text, res_keyword text, res_detail text, res_speed text,web_link text, visited integer, ctime text)")
		s_conn.commit()
		cursor.close()
		s_conn.close()
	except:
		pass


#设置全局变量
define("port", default="8888", help="set tornado server port")
define("debug", default=True, help="set debug")
options.parse_command_line() 



if __name__ == "__main__":

    #使用传进来的端口
    if len(sys.argv) > 1:
        options.port = sys.argv[1]
    application.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()