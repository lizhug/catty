#coding:utf-8
import requests
from handler import handler
import redis
import json
import threading
import time
r_server = redis.Redis("127.0.0.1")			##链接redis

def crawls(url):
	resHandler = handler()
    
	headers = { #伪装为浏览器抓取
   		'User-Agent':'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6'
    }

	try:
		r = requests.get(url, headers=headers)
		if r.status_code == 200 or r.status_code == 301:
			if r.encoding.lower() == "utf-8":
				pass
			elif r.encoding.lower() == "iso-8859-1":
				r.encoding = "gb18030"

			resHandler.resHandler(url, r.text, r.encoding) #, r.encoding
		else:
			pass
		#print ("==================status_code================" + r.status_code)
	except:
		pass#print ("+++++++++++++++++++++++++++++++++++Error+++++++++++++++++++++++++++++++++++")

def thread_fun(name):
	
	pass
	
class MyThread(threading.Thread):
	def __init__(self):
		threading.Thread.__init__(self)

	def run(self):
		#time.sleep(0.01)
		while r_server.llen("wb:crawl:wait") > 0:
			url = r_server.rpop("wb:crawl:wait")
			crawls(url)

if __name__ == "__main__":

	start_urls = []

	if r_server.llen("wb:crawl:wait") == 0:
		start_urls = ["http://btkitty.org",
			"http://magnet-torrent.com/",
			"http://www.torrentkitty.org/search/STAR-214/", 
			"http://www.mininova.org/"]
	else:
		start_urls.append(r_server.rpop("wb:crawl:wait"))
		start_urls.append(r_server.rpop("wb:crawl:wait"))
		start_urls.append(r_server.rpop("wb:crawl:wait"))
		start_urls.append(r_server.rpop("wb:crawl:wait"))

	for i,item in enumerate(start_urls):
		if isinstance(item, bytes):
			start_urls[i] = item.decode("utf-8")

		r_server.rpush("wb:crawl:wait", item)
		my_thread = MyThread()
		my_thread.start()


	
