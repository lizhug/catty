# -*- coding: utf-8 -*-
#coding:utf-8
import re
import sqlite3
import redis
import jieba
import sys
import json
import io
from tld import get_tld
import time
from bs4 import BeautifulSoup
sys.stdout = io.TextIOWrapper(sys.stdout.buffer,encoding='utf8')


r_server = redis.Redis("127.0.0.1")

class handler():

	html_encoding = "utf-8"		#默认吗文档编码

	def resHandler(self, url, resHtml, encoding):
		self.html_encoding = encoding

		keywords = ""
		description = ""
		web_title = ""

		####提取标签、及相关信息	
		soup = BeautifulSoup(resHtml)

		web_title = soup.title.string.encode(self.html_encoding).strip() if soup.title else "" 		##网页名称

		#print (chardet.detect(resHtml))

		#print (web_title)
		#f = open("ddd.txt", "wb+")
		#f.write(web_title)
		#f.close()


		#return

		for meta in soup.find_all("meta"):
			if meta.has_attr("name"):
				if meta['name'] == "description":	
					description = meta['content']		
		
		keywords = []
		##当网页的描述和标题都为空的时候不收录网页
		if description == "" and web_title == "":
			return		

		##倒排词语提取				
		elif description == "":
			keywords = list(set([i.upper() for i in list(jieba.cut(web_title)) if i.strip() != ""]))
		elif web_title == "":
			keywords = list(set([i.upper() for i in list(jieba.cut(description)) if i.strip() != ""]))
			web_title = description
		else:
			keywords = list(set([i.upper() for i in list(jieba.cut(web_title)) if i.strip() != ""]))

		#获取所有的a标签
		link = soup.find_all('a')

		for al in link:

			##获取链接地址
			str_link = al.get('href')		
			res_name = web_title

			#print (type(str_link))
			if isinstance(str_link, type(None)):
				return

			if re.search("^magnet(.*)", str_link):			#匹配磁力链接
				self._filePipline({
					"res_link": str_link,
					"res_name": res_name,
					'web_link': url,
					"res_keywords": keywords,
					"type": 1
				})
			elif re.search("^thunder://(.*)", str_link, flags=re.I):			#匹配迅雷链接
				self._filePipline({
					"res_link": str_link,
					"res_name": res_name,
					'web_link': url,
					"res_keywords": keywords,
					"type": 2
				})

			elif re.search("^ed2k://(.*)", str_link, flags=re.I):			#匹配电驴链接
				self._filePipline({
					"res_link": str_link,
					"res_name": res_name,
					'web_link': url,
					"res_keywords": keywords,
					"type": 3
				})

			elif re.search("^http(.*)\.torrent$", str_link, flags=re.I):			#匹配torrent链接
				self._filePipline({
					"res_link": str_link,
					"res_name": res_name,
					'web_link': url,
					"res_keywords": keywords,
					"type": 4
				})

			elif re.search("^http://", str_link, flags=re.I):				##网页文件（有可能是动态文件链接）
				self._linkDB(str_link)
			elif re.search("^/", str_link, flags=re.I):				##网页文件、需要添加域名和协议
				domain = ""
				if isinstance(url, bytes):
					domain = url.decode(self.html_encoding).split("/")[2]
				elif isinstance(url, str):
					domain = url.split("/")[2]
				else:
					return

				str_link = "http://" + domain + str_link

				self._linkDB(str_link)


	def _filePipline(self, info):
		item = {}
		#magnet种子文件
		#if info['type'] == 1:
		if isinstance(info['res_name'], bytes):
			item['res_name'] = info['res_name'].decode(self.html_encoding)
		else:
			item['res_name'] = info['res_name']

		item['res_size'] = 0

		if isinstance(info['res_link'], bytes):
			item['res_link'] = info['res_link'].decode(self.html_encoding)
		else:
			item['res_link'] = info['res_link']

		item['res_speed'] = 200
		item['res_keyword'] = info['res_keywords']

		# for i, v in enumerate(item['res_keyword']):
		# 	f = open("ddd.txt", "ab+")
		# 	if isinstance(v, bytes):
		# 		f.write(v.encode(self.html_encoding))
		# 		f.close()
		# 		print ("=================")
		# 	else:
		# 		pass

		item['res_detail'] = ""
		if isinstance(info['web_link'], bytes):
			item['web_link'] = info['web_link'].decode(self.html_encoding)
		else:
			item['web_link'] = info['web_link']
		item['visited'] = 0
		item['ctime'] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
		item['type'] = info['type']
		#elif info['type'] == 2:
		#print (item)
		self._resDB(item)
	##把资源存进数据库与缓存
	def _resDB(self, item):
		#print ("+++++++++++++++++++++++++++++++++++++++++++++")
		#插入资源链接到数据库
		

		if r_server.exists("res:" + self.md5(item.get("res_link")) + ":link"):
			pass
		else:
			conn = sqlite3.connect("../test.db")
			conn.row_factory = sqlite3.Row
			cursor = conn.cursor()
		
			cursor.execute("insert into res(type,res_name,res_link, res_size,res_keyword,res_speed,web_link,visited,ctime) values(?,?,?,?,?,?,?,?,?)",(1, item.get("res_name"),item.get("res_link"), item.get("res_size"), json.dumps(item.get("res_keyword")),item.get("res_speed"),item.get("web_link"), 0, time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())))
			resId = cursor.lastrowid
			conn.commit()
			conn.close()
			
			##关键词倒排插入
			for j in item['res_keyword']:
				r_server.sadd("key:" + j + ":id", resId)

			r_server.set("res:" + self.md5(item.get("res_link")) + ":link", 1)		#标记资源


		#标记该网页10天内不用再查找
		r_server.set("wb:" + self.md5(item.get('web_link')) + ":visited", 1)
		r_server.expire("wb:" + self.md5(item.get('web_link')) + ":visited", 3600 * 24 * 10)

	##把待爬取的链接存进缓存和数据库
	def _linkDB(self, url):
		r_server = redis.Redis("127.0.0.1")
		
		if r_server.exists("wb:" + self.md5(url) + ":visited"):
			pass
		else:
			#标记网页不用重复爬取
			r_server.set("wb:" + self.md5(url) + ":visited", 1)
			r_server.expire("wb:" + self.md5(url) + ":visited", 3600 * 24 * 10)
			###当key不存在，把网址push进待爬取列表
			r_server.lpush("wb:crawl:wait", url)


	def md5(self, my_str):
		import hashlib
		m = hashlib.md5()
		if isinstance(my_str, str):
			m.update(my_str.encode(self.html_encoding))
			#print ("md5===============")
		elif isinstance(my_str, bytes):
			m.update(my_str.decode(self.html_encoding).encode(self.html_encoding))
			#print ("md5------------------")
		else:
			pass
			#print ("????????????????????????")

		return m.hexdigest()